import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PendingForReviewComponent } from './admin/pending-for-review/pending-for-review.component';
import { LoginComponent } from './admin/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component'
import { from } from 'rxjs';
import{} from './admin/home/home.component';
import { ApprovedApplicationsComponent } from './admin/approved-applications/approved-applications.component';
import { RejectedApplicationsComponent } from './admin/rejected-applications/rejected-applications.component';
import { ReturnedApplicationsComponent } from './admin/returned-applications/returned-applications.component';
import { AuthGaurdService } from './service/auth-gaurd.service';
import { LogoutComponent } from './admin/logout/logout.component';
import { ViewForApprovedDetailsComponent} from './admin/view-for-approved-details/view-for-approved-details.component';
import { ViewForPendingReviewComponent } from './admin/view-for-pending-review/view-for-pending-review.component';
import { ViewForRejectDetailsComponent } from './admin/view-for-reject-details/view-for-reject-details.component';
import { ViewForReturnDetailsComponent } from './admin/view-for-return-details/view-for-return-details.component';
import { ChangePasswordComponent } from './admin/change-password/change-password.component';
import { LucComponent} from './admin/luc/luc.component'


const appRoutes: Routes = [
  { path: '', redirectTo: "logout", pathMatch: "full" },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGaurdService] },
  {path: 'change_password',component:ChangePasswordComponent,canActivate:[AuthGaurdService]},
  { path: 'pending_for_review', component: PendingForReviewComponent, canActivate: [AuthGaurdService] },
  { path: 'rejected_applications', component: RejectedApplicationsComponent, canActivate: [AuthGaurdService] },
  { path: 'approved_applications', component: ApprovedApplicationsComponent, canActivate: [AuthGaurdService] },
  { path: 'returned_applications', component: ReturnedApplicationsComponent, canActivate: [AuthGaurdService] },
  { path: 'view_for_approved_details',component:ViewForApprovedDetailsComponent,canActivate:[AuthGaurdService]},
  { path: 'view_for_pending_review', component: ViewForPendingReviewComponent, canActivate: [AuthGaurdService] },
  { path: 'view_for_reject_details', component: ViewForRejectDetailsComponent, canActivate: [AuthGaurdService] },
  { path: 'view_for_return_details',component:ViewForReturnDetailsComponent,canActivate:[AuthGaurdService]},
  { path: 'luc',component:LucComponent,canActivate:[AuthGaurdService]},
  { path: 'logout', component: LogoutComponent }

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
