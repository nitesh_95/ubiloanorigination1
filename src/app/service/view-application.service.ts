import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ViewApplicationService implements OnInit {
  private pendingCustomerData :any= []
  private approveCustomerData :any= []
  private rejectCustomerData :any= []
  private returnCustomerData :any= []

  constructor() { }
ngOnInit(){}

  pendingViewdata(pendingCustomerData:any) {
    this.pendingCustomerData = pendingCustomerData;
 
    sessionStorage.setItem('pendingCustomerData',JSON.stringify(this.pendingCustomerData))
    
  }
  approveViewdata(approveCustomerData:any) {
    this.approveCustomerData = approveCustomerData;
    sessionStorage.setItem('approveCustomerData',JSON.stringify(this.approveCustomerData))

  }
  rejectViewdata(rejectCustomerData:any) {
    this.rejectCustomerData = rejectCustomerData;
    sessionStorage.setItem('rejectCustomerData',JSON.stringify(this.rejectCustomerData))

  }
  returnViewdata(returnCustomerData:any) {
    this.returnCustomerData = returnCustomerData;
    sessionStorage.setItem('returnCustomerData',JSON.stringify(this.returnCustomerData))

  }
}


