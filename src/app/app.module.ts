import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { SliderComponent } from './admin/slider/slider.component';
import { PendingForReviewComponent } from './admin/pending-for-review/pending-for-review.component';
import { ApprovedApplicationsComponent } from './admin/approved-applications/approved-applications.component';
import { ReturnedApplicationsComponent } from './admin/returned-applications/returned-applications.component';
import { RejectedApplicationsComponent } from './admin/rejected-applications/rejected-applications.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LogoutComponent } from './admin/logout/logout.component';
import { LoginComponent } from './admin/login/login.component';
import { ChangePasswordComponent } from './admin/change-password/change-password.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './admin/home/home.component';
import { HeaderComponent } from './admin/header/header.component';
import { FooterComponent } from './admin/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { AngularDateTimePickerModule} from 'angular2-datetimepicker';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserIdleModule } from 'angular-user-idle';
import { MyDatePickerModule } from 'mydatepicker';
import { ChartsModule } from 'ng2-charts';
import { ViewForApprovedDetailsComponent} from './admin/view-for-approved-details/view-for-approved-details.component';
import { ViewForPendingReviewComponent } from './admin/view-for-pending-review/view-for-pending-review.component';
import { ViewForRejectDetailsComponent } from './admin/view-for-reject-details/view-for-reject-details.component';
import { ViewForReturnDetailsComponent } from './admin/view-for-return-details/view-for-return-details.component';
import { LucComponent } from './admin/luc/luc.component';
@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    SliderComponent,
    PendingForReviewComponent,
    ApprovedApplicationsComponent,
    ReturnedApplicationsComponent,
    RejectedApplicationsComponent,
    DashboardComponent,
    LogoutComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ViewForApprovedDetailsComponent,
    ViewForPendingReviewComponent,
    ViewForRejectDetailsComponent,
    ViewForReturnDetailsComponent,
    ChangePasswordComponent,
    LucComponent
  ],
  imports: [
    MyDatePickerModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    FormsModule,
    RouterModule, 
    AngularDateTimePickerModule,
    ChartsModule,
     UserIdleModule.forRoot({idle: 27000, timeout: 27000, ping: 27000})
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
		NO_ERRORS_SCHEMA
	]
})
export class AppModule {
  
 }
