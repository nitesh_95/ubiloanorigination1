import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';

@Component({
  selector: 'app-pending-for-review',
  templateUrl: './pending-for-review.component.html',
  styleUrls: ['./pending-for-review.component.css']
})
export class PendingForReviewComponent implements OnInit {
  constructor(private http: HttpClient,
    private countService: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router: Router) {
  }
  pendingForReview = []
  pendingForReviewCount: number;
  common_IP :any;
  
  ngOnInit() {
    let t = this;
    
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    setTimeout(function () {
      t.getData(event)
    }, 1000)

  }

  searchText;

  p: number = 1;
  pendingCustomerData = [];
  private pageSize: number = 5;
  getData(event) {
    
const base_URL = this.common_IP+`/fetchdata-Service/getPendingForReviewData`;
    this.http.post(base_URL, {
      solid: 1247

    }).subscribe((data) => {
      console.log(this.pendingCustomerData)
      this.pendingForReview.push(data)
      this.pendingForReview = this.pendingForReview[0]
      console.log(this.pendingForReview)
    })
  }
  getImage(event) {

  }
  fetchData(event) {
    this.pendingCustomerData = []
    var selected_id = event.currentTarget.id
    this.pendingForReview.forEach(data => {
      if (selected_id == data.applicationid) {
        this.pendingCustomerData.push(data)
        this.viewApplication.pendingViewdata(this.pendingCustomerData)
        this.router.navigateByUrl('/view_for_pending_review')
      }
    })
  
  }
}

// 'http://172.18.1.169:8081/documments-Service/customer/getimage?solid=1247&applicationid='+this.applicationid+'&pdfType=ApplicantImage'