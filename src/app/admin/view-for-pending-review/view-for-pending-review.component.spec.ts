import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForPendingReviewComponent } from './view-for-pending-review.component';

describe('ViewForPendingReviewComponent', () => {
  let component: ViewForPendingReviewComponent;
  let fixture: ComponentFixture<ViewForPendingReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewForPendingReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForPendingReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
