import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";
import { routerNgProbeToken } from '@angular/router/src/router_module';
declare var $ : any;
@Component({
  selector: 'app-view-for-pending-review',
  templateUrl: './view-for-pending-review.component.html',
  styleUrls: ['./view-for-pending-review.component.css']
})
export class ViewForPendingReviewComponent implements OnInit {

  id: any;
  applicantId: any;
  applicantName: any;
  solid: number;
  documentValue: any;
  filename: any;
  dateOfBirth: any
  mobile: any
  address: any
  loanAmount: any
  noOfMonths: any
  emi: any
  bmLoanAmount: any
  bmNoOfMonths: any
  bmEmi: any
  savingAccount: any
  confirmSavingAccount: any
  bankCustomerId: any
  confirmbankCustomerId: any
  remarks: any
  bankstatus: any
  applicationid: any
  rateofInterest: any
  showMe: any = false;
  clickedTargetId: any;
  clickedTargetValue: any;
  
  pendingCustomerData: any = [];
  imagesrc :string;
  applicantID : number;
  googlemaphousemap:any;
  googlemapbusinessmap:any;
  max:any
  roi:any;
  aadharNumber:any;
  common_IP :any;
  rate:any
  constructor(private http: HttpClient,
    private router: Router, ) { }
  ngOnInit() {
    this.id = JSON.parse(sessionStorage.getItem('username'))
    
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))  
    
    this.pendingCustomerData = JSON.parse(sessionStorage.getItem('pendingCustomerData'))
    this.pendingCustomerData.forEach(data => {
      this.applicantID = data.applicationid
      console.log(data)
      if(isNaN(data.addressproof) ) {
        this.aadharNumber = '-------------'
      } 
      else {
        this.aadharNumber = data.addressproof
      }
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale 
      var rateOfInt = mclr*1 + mclrmalefemale*1
      this.roi = rateOfInt.toFixed(2)
      console.log(this.roi)
      this.googlemaphousemap = 'http://maps.google.com/?q='+data.permhouseno +','+data.permstreetno+','+data.permlandmark+','+data.permdistrict+','+data.permstate+','+data.permpincode;
      this.googlemapbusinessmap = 'http://maps.google.com/?q='+data.commhouseno+','+data.commstreetno +','+data.commlandmark+','+','+data.CommDistrict+','+data.commstate+','+data.permpincode;
    })
    this.imagesrc = this.common_IP+'/documments-Service/customer/getimage?solid=1247&applicationid='+this.applicantID+'&pdfType=ApplicantPhoto'
 
    
  }

  matchingAccount(event) {
    if ($('#savingAccount').val()=='' || $('#confirmsavingAccount').val() == '') {
      $('#message').html('').css('color', 'white');
    } else if($('#savingAccount').val() == $('#confirmsavingAccount').val() ) {
      $('#message').html('Matching').css('color', 'green');
    }else{
      $('#message').html('Not Matching').css('color', 'red');
    }

  }
  matchingBankcustId(event) {
    if ($('#bankCustId').val() == '' || $('#confirmbankCustId').val() =='') {
      $('#nextMessage').html('').css('color', 'white');
    } else if($('#bankCustId').val() ==  $('#confirmbankCustId').val() ){
      $('#nextMessage').html('Matching').css('color', 'green');
    }else{
      $('#nextMessage').html('Not Matching').css('color', 'red');
    }
  }
  modalPopup(event) {

  }


  calculator(event) {
   
      var principalAmt = +$("#loanAmount").val();
      var uhRecommendedAmt = +$('#uhRecommendedAmt').val()
      var rate = $("#RateOfIntrest").val();
      var term = $("#NoOfMonths").val();
      var r = parseFloat(rate);
      var t = parseFloat(term);
      var R = (r / (12)) / 100;
      
      var e = (principalAmt * R * (Math.pow((1 + R), t)) / ((Math.pow((1 + R), t)) - 1));
      if(principalAmt > uhRecommendedAmt){
        alert("Please Enter Loan Amount Less then or Equal to Subk Recommended Loan Amount")
        return false;
      }else{
        console.log(uhRecommendedAmt)
      var finalamount = Math.round(e);
      console.log(finalamount)
      $("#emiAmt").val(Math.round(e));
      }
  }

  


 
  downloadPdf(event) {
    this.pendingCustomerData.forEach(data => {
      this.applicantName = data.appl1name
    });
    this.applicationid = event.currentTarget.id
   
    this.documentValue = event.srcElement.attributes.value.value;
    console.log(this.documentValue)
    this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicantId
    const base_url = this.common_IP+'/documments-Service/customer/generatepdf'
    return this.http.post(
     base_url,  {
      solid: this.id,
      applicationid: this.applicationid,
      pdfType: this.documentValue
    },
      { responseType: 'blob' }
    ).subscribe((item => {
      console.log(this.applicationid)
      var headers = item;
      saveAs(item, this.filename + ".pdf");
    }));
  }

 

  getclickedId(event) {
    this.bmLoanAmount = (<HTMLInputElement>document.getElementById("loanAmount")).value;
    this.bmEmi = (<HTMLInputElement>document.getElementById("emiAmt")).value;
    this.bmNoOfMonths = (<HTMLInputElement>document.getElementById("NoOfMonths")).value;
    this.remarks = (<HTMLInputElement>document.getElementById("remarks")).value;
   
    this.clickedTargetValue = event.currentTarget.value
    this.clickedTargetId = event.currentTarget.id
  
    if ( (this.bmLoanAmount == '') || (this.bmNoOfMonths =='')){
     
      alert("Please enter all the Fields")
    }else{
  
        $("#modalPopup").modal('show');

    this.clickedTargetId = event.currentTarget.id
    if(this.clickedTargetValue = 'Approve') {
      this.clickedTargetValue = event.currentTarget.value
  
     }else if(this.clickedTargetValue = 'Reject') {
      this.clickedTargetValue = event.currentTarget.value
  
     }
     else {
      this.clickedTargetValue = event.currentTarget.value

     }
     
   

    }
    // } else if (this.clickedTargetValue == 'Reject') {
    //   this.bmLoanAmount = (<HTMLInputElement>document.getElementById("loanAmount")).value;
    //   this.bmEmi = (<HTMLInputElement>document.getElementById("emiAmt")).value;
    //   this.bmNoOfMonths = (<HTMLInputElement>document.getElementById("NoOfMonths")).value;
    //   this.remarks = (<HTMLInputElement>document.getElementById("remarks")).value;
    //   $('.bank_status').css({ "background-color": "#ac2925", "color": "#fff" })
    // } else {
    //   this.bmLoanAmount = (<HTMLInputElement>document.getElementById("loanAmount")).value;
    //   this.bmEmi = (<HTMLInputElement>document.getElementById("emiAmt")).value;
    //   this.bmNoOfMonths = (<HTMLInputElement>document.getElementById("NoOfMonths")).value;
    //   this.remarks = (<HTMLInputElement>document.getElementById("remarks")).value;
    //   $('.bank_status').css({ "background-color": "#d58512", "color": "#fff" })
    // }
  }
  downloadzip(event) {
    console.log(event)
    this.applicantId = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename = this.documentValue + "_" + this.applicantId
    console.log(this.filename)
    const base_url =  this.common_IP+'/documments-Service/customer/getzip?solid=1247&applicationid='+this.applicantId;
    return this.http.post(
      base_url,
      {
        pdfType: this.documentValue,
      },
      { responseType: 'blob' }
    ).subscribe((item => {
  
      console.log(base_url)
      console.log(item.size)
      if(item.size == 0){
        alert("The Required Zip file is not Available")
      }else{
      var headers = item;
      saveAs(item, this.filename + ".zip");
    }}));
  }
  downloadCibil(event) {
    console.log(event)
    this.applicationid = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename =  this.documentValue + "_" + this.applicationid
    const base_url = this.common_IP+'/documments-Service/customer/getpdf?solid=1247&applicationid='+this.applicationid+'&pdfType='+this.documentValue
    return this.http.post(
      base_url,
      {
        pdfType: this.documentValue,
      },
      { responseType: 'blob' }
    ).subscribe((item => {
      if (item.size == 0) {
        alert("The Required Pdf Does not exist")
      }else {
        var headers = item;
        saveAs(item, this.filename + ".pdf");
      } 
    }));
  }

  getAllData(event) {
    console.log(this.showMe)
    var targetId = event.currentTarget.id
    this.applicationid
    const base_url = this.common_IP+'/approval-service/users/submit'
    this.pendingCustomerData.forEach(data => {
      this.applicantId = data.applicationid
      this.applicantName = data.appl1name
      this.dateOfBirth = data.dobdate
      this.mobile = data.mobileno
      this.address = data.appl1address
      this.loanAmount = data.uhRecommendedLoanamount
      this.noOfMonths = data.uhRecommendedNoofmonths

      this.emi = data.uhRecommendedInstalmentamount
      this.rateofInterest = data.mclr*1 + data.mclrmalefemale*1,
      this.rate = this.rateofInterest.toFixed(2)
      console.log(this.rate)
      
        this.bmLoanAmount = (<HTMLInputElement>document.getElementById("loanAmount")).value;
      this.bmEmi = (<HTMLInputElement>document.getElementById("emiAmt")).value;
      this.bmNoOfMonths = (<HTMLInputElement>document.getElementById("NoOfMonths")).value;
      this.remarks = (<HTMLInputElement>document.getElementById("remarks")).value;
      console.log(this.bmNoOfMonths)

      if ((this.bmLoanAmount != '') && (this.bmNoOfMonths != '')) {
        
        if (this.showMe == false) {
          this.confirmSavingAccount = '';
          this.confirmbankCustomerId = '';
        }
        else {
          this.confirmSavingAccount = (<HTMLInputElement>document.getElementById("confirmsavingAccount")).value;
          this.confirmbankCustomerId = (<HTMLInputElement>document.getElementById("confirmbankCustId")).value;
        }
        
        this.http.post(base_url, {
          solid: this.id,
          applicationid: this.applicantId,
          bankcustid: this.confirmbankCustomerId,
          bankremarks: this.remarks,
          loanamountsactioned: this.bmLoanAmount,
          instalmentamount: this.bmEmi,
          savingaccnumber: this.confirmSavingAccount,
          noofinstalments: this.bmNoOfMonths,
          bankverificationstatus: targetId,
          interestrate: this.rate 
         
        }).subscribe(data => {
         console.log(data)
          console.log(targetId)
          
          if (targetId == 'Approved') {
         
            alert("Data Updated Successfully|| Moved to Approved Applications")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else if (targetId == 'Returned') {
           
            alert("Application Returned Successfully|| Moved to Returned Applications")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else {
        
            alert("Application Rejected|| Moved to Rejected Applications")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          }
        })
      } else {
        alert('Please enter the loan amount and no of months ')
      }
    });
  }
}
