import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
import { saveAs } from "file-saver";
@Component({
  selector: 'app-luc',
  templateUrl: './luc.component.html',
  styleUrls: ['./luc.component.css']
})
export class LucComponent implements OnInit {

  constructor(private http: HttpClient,
    private approvedLoans: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router: Router) {

  }
  lucApplications: any = []
  lucApplicationsCount: any;
  approvedForLoans: number;
  dropdownvalues: any = 5;
  paginationNumber: any = 0;
  
  common_IP :any;
  
  ngOnInit() {
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.getData()
    this.getDataCount();
  }
  getdropdownvalue(event) {

    let targetValue = event.currentTarget.value;
    let splitValue = targetValue.split(': ')
    this.paginationNumber = splitValue[0];
    this.dropdownvalues = splitValue[1]
    console.log(this.dropdownvalues)
    this.getData()
  }

  searchText;


  p: number = this.paginationNumber;
  d: number = this.lucApplicationsCount;
  applicationId: number;
  documentValue: any;
  filename: any;


  public pageSize: number = this.dropdownvalues;
  approveCustomerData = [];
  downloadPdf(event) {
    console.log(event)
    this.applicationId = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename = this.documentValue + "_" + this.applicationId
    console.log(this.filename)
    console.log(this.documentValue)
    const base_URL = this.common_IP+'/documments-Service/customer/getpdf?solid=1247&applicationid='+this.applicationId+'&pdfType='+this.documentValue
    return this.http.post(base_URL,
      {

        pdfType: this.documentValue,
      },
      { responseType: 'blob' }
    ).subscribe((item => {
      console.log(base_URL)
      console.log(this.applicationId)
      if (item.size == 0) {
        alert("The Required Pdf Does not exist")
      } else {
        console.log(item.size)
        var headers = item;
        saveAs(item, this.filename + ".pdf");
      }
    }));
  }


  getData() {
    this.lucApplications = []
    const base_URL = this.common_IP+'/fetchdata-Service/customer?pageSize='+this.dropdownvalues;
    console.log(base_URL)
    return this.http.post(base_URL, {
    }).subscribe((data) => {
      this.lucApplications.push(data)
      this.lucApplications = this.lucApplications[0]
      console.log(this.lucApplications)
    })
  }

  getDataCount() {
    this.lucApplicationsCount = []
    const base_URL = this.common_IP+'/fetchdata-Service/customer/count';
    console.log(base_URL)
    return this.http.post(base_URL, {
    }).subscribe((data) => {

      this.lucApplicationsCount = data
      console.log(this.lucApplicationsCount)
    })
  }

}

