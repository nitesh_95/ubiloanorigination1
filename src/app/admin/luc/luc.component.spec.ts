import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LucComponent } from './luc.component';

describe('LucComponent', () => {
  let component: LucComponent;
  let fixture: ComponentFixture<LucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
