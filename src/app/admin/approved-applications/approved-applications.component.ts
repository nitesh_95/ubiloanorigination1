import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-approved-applications',
  templateUrl: './approved-applications.component.html',
  styleUrls: ['./approved-applications.component.css']
})
export class ApprovedApplicationsComponent implements OnInit {


  constructor(private http: HttpClient,
    private approvedLoans: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router: Router) {

  }
  approvedApplications: any = []
  approvedForLoans: number;
  common_IP :any;
  ngOnInit() {
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.getData(event)
    
    console.log(this.common_IP)
  }
  searchText;


  p: number = 1;
  private pageSize: number = 5;
  approveCustomerData = [];
  getData(event) {
    const base_url = this.common_IP+`/fetchdata-Service/getApprovedApplications`
    console.log(base_url)
    
    this.http.post(base_url, {
    solid: 1247
    }).subscribe((data) => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
      console.log(this.approvedApplications)

    })
  }
  fetchData(event) {
    console.log(event)
    this.approveCustomerData = []
    var selected_id = event.currentTarget.id
    this.approvedApplications.forEach(data => {
      if (selected_id == data.applicationid) {
        this.approveCustomerData.push(data)
        console.log(this.approveCustomerData)
        this.viewApplication.approveViewdata(this.approveCustomerData)
        this.router.navigate(['view_for_approved_details']);
      }
    })
  }
}





