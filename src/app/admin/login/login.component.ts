import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { from } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {

  username = ''
  password :any;
  invalidLogin = false

  common_IP :any;
  constructor(private router: Router,
    private loginservice: AuthenticationService,
    private passService : AuthPassService,
    private http: HttpClient,) { }

    checkLogin(event) {
      this.username = event.target.elements[0].value;
      sessionStorage.setItem('username', this.username)
      this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
      this.password = event.target.elements[1].value;
      const base_URL = 'http://172.18.1.169:8081/login-Service/login?UserId='+this.username+'&Password='+this.password;
      console.log(base_URL)
     return this.http.post(base_URL , {}).subscribe(data => {
       console.log(data)
       if(data['status'] == '00') {
        this.router.navigate(['dashboard']).then(() => {
          window.location.reload();
        });
      }
    })
    }
   } 