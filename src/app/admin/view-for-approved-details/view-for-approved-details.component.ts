import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";

@Component({
  selector: 'app-view-for-approved-details',
  templateUrl: './view-for-approved-details.component.html',
  styleUrls: ['./view-for-approved-details.component.css']
})
export class ViewForApprovedDetailsComponent implements OnInit {
  applicationid: any;
  constructor(private http: HttpClient, ) { }
  approveCustomerData:any = [];
  common_IP :any;
  googlemaphousemap:any;
  googlemapbusinessmap:any;
  imagesrc:any;
  roi:any;
  aadharNumber:any
  ngOnInit() {
    
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.approveCustomerData = JSON.parse(sessionStorage.getItem('approveCustomerData'))
    console.log(this.approveCustomerData)
    this.approveCustomerData.forEach(data => {
      this.applicationid = data.applicationid
      console.log(data)
      if(isNaN(data.addressproof) ) {
        this.aadharNumber = '-------------'
      } 
      else {
        this.aadharNumber = data.addressproof
      }
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale 
      var rateOfInt = mclr*1 + mclrmalefemale*1
      this.roi = rateOfInt.toFixed(2)
      console.log(this.roi)
      this.googlemaphousemap = 'http://maps.google.com/?q='+data.permhouseno +','+data.permstreetno+','+data.permlandmark+','+data.permdistrict+','+data.permstate+','+data.permpincode;
      this.googlemapbusinessmap = 'http://maps.google.com/?q='+data.commhouseno+','+data.commstreetno +','+data.commlandmark+','+','+data.CommDistrict+','+data.commstate+','+data.permpincode;
    })
    this.imagesrc = this.common_IP+'/documments-Service/customer/getimage?solid=1247&applicationid='+this.applicationid+'&pdfType=ApplicantPhoto'
  }
  
  applicantId: number;
  applicantName: any;
  solid: number;
  documentValue: any;
  filename: any;
  downloadzip(event) {
    console.log(event)
    this.applicantId = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename = this.documentValue + "_" + this.applicantId
    console.log(this.filename)
    const base_url =  this.common_IP+'/documments-Service/customer/getzip?solid=1247&applicationid='+this.applicantId;
    return this.http.post(
      base_url,
      {
        pdfType: this.documentValue,
      },
      { responseType: 'blob' }
    ).subscribe((item => {
  
      console.log(base_url)
      console.log(item.size)
      if(item.size == 0){
        alert("The Required Zip file is not Available")
      }else{
      var headers = item;
      saveAs(item, this.filename + ".zip");
    }}));
  }
  downloadCibil(event) {
    
    this.applicationid = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename =  this.documentValue + "_" + this.applicationid
  const base_url = this.common_IP+'/documments-Service/customer/getpdf?solid=1247&applicationid='+this.applicationid+'&pdfType='+this.documentValue
    return this.http.post(
      base_url,
      {
        pdfType: this.documentValue,
      },
      { responseType: 'blob' }
    ).subscribe((item => {
      if (item.size == 0) {
        alert("The Required Pdf Does not exist")
      }else {
        var headers = item;
        saveAs(item, this.filename + ".pdf");
      } 
    }));
  }

    downloadPdf(event) {
    this.approveCustomerData.forEach(data => {
      this.applicantName = data.appl1name
    });
    this.applicationid = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicationid
    const base_url = this.common_IP+'/documments-Service/customer/generatepdf'
    return this.http.post(
      base_url,
      {
      solid: 1247,
      applicationid: this.applicationid,
      pdfType: this.documentValue,
    },
      { responseType: 'blob' }
    ).subscribe((item => {
      var headers = item;
      saveAs(item, this.filename + ".pdf");
    }));
  }

}

