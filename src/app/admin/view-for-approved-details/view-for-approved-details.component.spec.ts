import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForApprovedDetailsComponent } from './view-for-approved-details.component';

describe('ViewForApprovedDetailsComponent', () => {
  let component: ViewForApprovedDetailsComponent;
  let fixture: ComponentFixture<ViewForApprovedDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewForApprovedDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForApprovedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
