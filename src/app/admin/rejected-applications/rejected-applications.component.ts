import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rejected-applications',
  templateUrl: './rejected-applications.component.html',
  styleUrls: ['./rejected-applications.component.css']
})
export class RejectedApplicationsComponent implements OnInit {

  constructor(private http: HttpClient, 
    private rejectedLoans: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router:Router) {

  }
  rejectedApplications: any = []
  rejectedForLoans: number;
  common_IP :any;
  
  ngOnInit() {
    
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.getData(event)
  }
  searchText;
 

  p: number = 1;
  private pageSize: number = 5;
  rejectCustomerData = [];
  getData(event) {
    
    const base_URL = this.common_IP+`/fetchdata-Service/getRejectedApplications`
     this.http.post(base_URL, {
      solid: 1247
    }).subscribe((data) => {
      this.rejectedApplications.push(data)
      this.rejectedApplications = this.rejectedApplications[0]
      
    })
  }
  fetchData(event) {
    console.log(event)
    this.rejectCustomerData = []
    var selected_id = event.currentTarget.id
    this.rejectedApplications.forEach(data => {
      if(selected_id == data.applicationid) {
        this.rejectCustomerData.push(data)
        console.log(this.rejectCustomerData)
        this.viewApplication.rejectViewdata(this.rejectCustomerData)
        this.router.navigate(['view_for_reject_details']);
      }
    })
  }
}





