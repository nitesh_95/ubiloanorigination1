import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-returned-applications',
  templateUrl: './returned-applications.component.html',
  styleUrls: ['./returned-applications.component.css']
})
export class ReturnedApplicationsComponent implements OnInit {

  constructor(private http: HttpClient,
    private returnedLoans:AuthPassService,
    private viewApplication: ViewApplicationService,
    private router:Router) {

  }
  returnedApplications: any = []
  returnedForLoans:number;
  common_IP :any;
  
  ngOnInit() {
    
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.getData(event)
  }
  searchText;
  p: number = 1;
  private pageSize: number = 5;
  returnCustomerData = [];
  getData(event) {
   

    const base_URL = this.common_IP+`/fetchdata-Service/getReturnedApplications`
    this.http.post(base_URL, {
      solid: 1247
    }).subscribe((data) => {
      this.returnedApplications.push(data)
      this.returnedApplications = this.returnedApplications[0]
      
    })
  }
  fetchData(event) {
    this.returnCustomerData = []
    var selected_id = event.currentTarget.id
    this.returnedApplications.forEach(data => {
      if(selected_id == data.applicationid) {
        this.returnCustomerData.push(data)
        console.log(this.returnCustomerData)
        this.viewApplication.returnViewdata(this.returnCustomerData)
        this.router.navigate(['view_for_return_details']);
      }
    })
  }
}





