import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForReturnDetailsComponent } from './view-for-return-details.component';

describe('ViewForReturnDetailsComponent', () => {
  let component: ViewForReturnDetailsComponent;
  let fixture: ComponentFixture<ViewForReturnDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewForReturnDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForReturnDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
