import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { AuthPassService } from 'src/app/service/auth-pass.service'
import { IMyDpOptions,IMyDate,IMyDateModel } from 'mydatepicker';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { JsonPipe } from '@angular/common';


@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	BarChart: any = [];
	PieChart: any = [];
	data: Date = new Date();
	id: any;
	common_IP :any;
	
	approvedLoan: any;
	pendingForReview: any;
	rejectedLoan: any;
	returnedLoan: any;
	today = new Date();
	
	public selDate1: IMyDate = {year: this.today.getFullYear(), month: this.today.getMonth() +1, day: 1};
	public selDate2: IMyDate = {year: this.today.getFullYear(), month: this.today.getMonth()+1 , day: this.today.getDate()};

	public myDatePickerOptions: IMyDpOptions = {
		dateFormat: 'dd/mm/yyyy',
		disableSince: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1},
		disableUntil: {year: this.today.getFullYear() - 1, month: this.today.getMonth() + 1, day: this.today.getDate() - 1}
	};
	
	public myDatePickerOption: IMyDpOptions = {
		dateFormat: 'dd/mm/yyyy',
		disableSince: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate() + 1}
	};
	
	public model: any = { date: { year: 2018, month: 10, day: 9 } };
	public date: any;
	public newDate :any;
	date1: any;
	date2: any;
	chartCountList:any = [];
	approvedCount:number;
	pendingCount:number;
	returnedCount:number;
	rejectedCount:number;
	fromDate:any = '';
	toDate:any = '';
	
	constructor(private authpass: AuthPassService,
		private dashboard: AuthPassService,
		private router: Router,
		private http: HttpClient,) {
			
		 }

	ngOnInit() {
	
	
		this.model = '';
		this.date = ''
	
		this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'));
	
		this.getChartCount(event)
		
		this.id = JSON.parse(sessionStorage.getItem('username'))
		let t = this;
		
		

		// setTimeout(function() {
		// 	t.charcount()
		// },1000)
	}

	chartcount() {
		
		this.chartCountList.forEach(data => {
			this.approvedCount = data.approvedCount;
			this.pendingCount = data.pendingCount;
			this.rejectedCount = data.rejectedCount;
			this.returnedCount = data.returnedCount;
		});
		this.BarChart = new Chart('barChart', {
			
			type: 'bar',
			data: {
				labels: ["Pending("+this.pendingCount+")", "Approved("+this.approvedCount+")", "Return("+this.returnedCount+")", "Reject("+this.rejectedCount+")"],
				datasets: [{
					data: [this.pendingCount, this.approvedCount , this.returnedCount, this.rejectedCount],
					backgroundColor: [
						'#4f81bc',
						'#9bbb58',
						'#23bfaa',
						'#c0504e',
					],
					borderColor: [
						'#4f81bc',
						'#9bbb58',
						'#23bfaa',
						'#c0504e'
					],
					borderWidth: 1
				}]
			},
			options: {
				title: {
					text: "Bar Chart",
					display: true
				},
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				},
				onClick: function (e) {
					
					var element = this.getElementAtEvent(e);
					var element_label = element[0]._view.label
					var element_label_split  = element_label.split("(")
					
					if (element_label_split[0] == 'Approved') {
					
						location.href = '/bmportal/#/approved_applications';
					}
					if (element_label_split[0] == 'Pending') {

						location.href = '/bmportal/#/pending_for_review';
					}
					if (element_label_split[0] == 'Return') {
						location.href = '/bmportal/#/returned_applications';
					}
					if (element_label_split[0] == 'Reject') {
						location.href = '/bmportal/#/rejected_applications';
					}
				}
			}
		});

	


		this.PieChart = new Chart('pieChart', {
			type: 'pie',
			data: {
				labels: ["Pending("+this.pendingCount+")", "Approved("+this.approvedCount+")", "Return("+this.returnedCount+")", "Reject("+this.rejectedCount+")"],
				datasets: [{

					data: [this.pendingCount, this.approvedCount , this.returnedCount, this.rejectedCount],
					backgroundColor: [
						'#4f81bc',
						'#9bbb58',
						'#23bfaa',
						'#c0504e',
					],
					borderColor: [
						'#4f81bc',
						'#9bbb58',
						'#23bfaa',
						'#c0504e',
					],
					borderWidth: 1
				}],

			},
			options: {
				title: {
					text: "Pie Chart",
					display: true
				},
				onClick: function (e) {
					
					var element = this.getElementAtEvent(e);
					var element_label = element[0]._view.label
					var element_label_split  = element_label.split("(")
					
					if (element_label_split[0] == 'Approved') {
					
						location.href = '/bmportal/#/approved_applications';
					}
					if (element_label_split[0] == 'Pending') {

						location.href = '/bmportal/#/pending_for_review';
					}
					if (element_label_split[0] == 'Return') {
						location.href = '/bmportal/#/returned_applications';
					}
					if (element_label_split[0] == 'Reject') {
						location.href = '/bmportal/#/rejected_applications';
					}
				}
			}

		});
		

	}
	onFromDateChanged(event: IMyDateModel): void {  
		this.fromDate = event.formatted;  
	};
	onToDateChanged(event: IMyDateModel): void {  
		this.toDate = event.formatted;  
	}; 
	getChartCount(event) {
	
		
		
		console.log(this.fromDate > this.toDate)
		if(this.fromDate > this.toDate)
		alert('From date should be smaller than To date')
		else {
		var solid = 1247
		const base_URL = this.common_IP+'/dashboard-Service/dashboard?From_Date='+this.fromDate+'&To_Date='+this.toDate+'&solId='+solid;
		console.log(base_URL)
		return this.http.post(base_URL , {
		}).subscribe(data => {
		
		this.chartCountList.push(data)
		
		this.chartcount()
		})
	}
	}

	



}
