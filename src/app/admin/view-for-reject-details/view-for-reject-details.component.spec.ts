import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForRejectDetailsComponent } from './view-for-reject-details.component';

describe('ViewForRejectDetailsComponent', () => {
  let component: ViewForRejectDetailsComponent;
  let fixture: ComponentFixture<ViewForRejectDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewForRejectDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForRejectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
