import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";

@Component({
  selector: 'app-view-for-reject-details',
  templateUrl: './view-for-reject-details.component.html',
  styleUrls: ['./view-for-reject-details.component.css']
})
export class ViewForRejectDetailsComponent implements OnInit {
  applicationid: any;
  constructor(private http: HttpClient, ) { }
  rejectCustomerData:any = [];
  common_IP :any;
  googlemaphousemap:any;
  googlemapbusinessmap:any;
  imagesrc:any;
  roi:any;
  aadharNumber:any;

  ngOnInit() {

 
    this.rejectCustomerData = JSON.parse(sessionStorage.getItem('rejectCustomerData'))
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP')) 
    this.rejectCustomerData.forEach(data => {
      this.applicationid = data.applicationid
      if(isNaN(data.addressproof) ) {
        this.aadharNumber = '-------------'
      } 
      else {
        this.aadharNumber = data.addressproof
      }
      console.log(data)
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale 
      var rateOfInt = mclr*1 + mclrmalefemale*1
      this.roi = rateOfInt.toFixed(2)
      console.log(this.roi)
      this.googlemaphousemap = 'http://maps.google.com/?q='+data.permhouseno +','+data.permstreetno+','+data.permlandmark+','+data.permdistrict+','+data.permstate+','+data.permpincode;
      this.googlemapbusinessmap = 'http://maps.google.com/?q='+data.commhouseno+','+data.commstreetno +','+data.commlandmark+','+','+data.CommDistrict+','+data.commstate+','+data.permpincode;
    })
    this.imagesrc = this.common_IP+'/documments-Service/customer/getimage?solid=1247&applicationid='+this.applicationid+'&pdfType=ApplicantPhoto'
  }
  applicantId: number;
  applicantName: any;
  solid: number;
  documentValue: any;
  filename: any;

  downloadPdf(event) {
    this.rejectCustomerData.forEach(data => {
      this.applicantName = data.appl1name
    });
    this.applicationid = event.currentTarget.id
    this.documentValue = event.srcElement.attributes.value.value
    console.log(this.documentValue)
    this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicationid
    const base_url = this.common_IP+'/documments-Service/customer/generatepdf'
    return this.http.post(
      base_url, 
      {
      solid: 1247,
      applicationid: this.applicationid,
      pdfType: this.documentValue,
    },
      { responseType: 'blob' }
    ).subscribe((item => {
      var headers = item;
      saveAs(item, this.filename + ".pdf");
    }));
  }

}