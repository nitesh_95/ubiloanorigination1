import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthPassService } from 'src/app/service/auth-pass.service'
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  jsonData: any;
  constructor(private router: Router, private http: HttpClient, private authpass: AuthPassService) {
  }
  currentPassword: any
  
  common_IP :any;
  ngOnInit() {
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.currentPassword = JSON.parse(sessionStorage.getItem('password'))
    console.log(this.currentPassword)
  }

  checkPassword(event) {


    let oldPassword = event.target.form.elements[0].value;
    let newPassword = event.target.form.elements[1].value;
    let confirmPassword = event.target.form.elements[2].value

    console.log(oldPassword);
    if (oldPassword == '' || newPassword == '' || confirmPassword == '') {
      alert("Please enter all the fields");

    } else if (oldPassword == newPassword) {
      alert("Old and new password are same")
      return false;
    } else if (newPassword != confirmPassword) {
      alert("New and Confirm Password do not match")

    } else {

      let olddPassword = oldPassword;
      let newwPassword = confirmPassword
      console.log(newwPassword)
      var solid = 1247
      const base_URL = this.common_IP+':8081/login-Service/changepassword?Solid=' + solid + '&oldPassword=' + olddPassword + '&newPassword=' + newwPassword;
      console.log(base_URL)

      return this.http.post(base_URL, {

      }).subscribe(data => {

        console.log(data)
        if (data['status'] == '01') {
          alert("Password not changed.Something went wrong please Try Again");
        } else if (data['status'] == '00') {
          alert("Password changed successfully. Please Login Again");
          window.location.href = "/login"
        } else {
          alert("Something went wrong Please try again")
        }
      })
    }
  }
}

